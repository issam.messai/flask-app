"""Sample Flask App"""
from flask import Flask

app = Flask(__name__)

# pylint: disable=missing-function-docstring

@app.route('/')
def index():
    return 'API index'

@app.route('/hello')
def hello():
    return 'Hello, World'
