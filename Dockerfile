FROM python:3.10-alpine

# install nginx
RUN apk add nginx
COPY nginx-vhost.conf /etc/nginx/http.d/default.conf
COPY frontend/ /usr/share/nginx/html/

# backend using gunicorn
WORKDIR /app
COPY app.py app.py
RUN pip install flask gunicorn
CMD ["sh", "-c", "nginx && gunicorn -b 0.0.0.0:8000 app:app"]

EXPOSE 80 8000
