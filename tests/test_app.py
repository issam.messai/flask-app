import unittest
from app import index

class TestApp(unittest.TestCase):

    def test_index(self):
        result = index()
        self.assertIsNotNone(result)
        self.assertIsInstance(result, str)
